package controller;

import java.io.*;
import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class ReadController {

    private static ReadController instance;

    private WriteController writeController;

    public synchronized static ReadController getInstance(){
        if(instance ==null){
            instance=new ReadController();
        }
        return instance;
    }

    public ReadController() {
        this.writeController = WriteController.getInstance();
    }


    public void getPage(String link) throws InterruptedException, IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        JBrowserDriver driver = new JBrowserDriver(Settings
                .builder().
                        timezone(Timezone.EUROPE_ATHENS).build());

        driver.get(link);
        driver.manage().window().maximize();

        int currentSize=driver.findElementsByClassName("_1Vpai").size();
        int desiredCountSize=100;
        while(desiredCountSize>currentSize){
            driver.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            Thread.sleep(1000);
            currentSize=driver.findElementsByClassName("_1Vpai").size();
        }

        String loadedPage = driver.getPageSource();
        Document document = Jsoup.parse(loadedPage);
        Elements elements = document.select("div._1Vpai");
        driver.quit();
        writeController.writeData(elements);

    }
}
