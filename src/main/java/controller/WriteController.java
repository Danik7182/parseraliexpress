package controller;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import domain.ItemAliexpress;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class WriteController {


    private static WriteController instance;

    public synchronized static WriteController getInstance(){
        if(instance ==null){
            instance=new WriteController();
        }
        return instance;
    }

    public WriteController() {
    }

    public void writeData(Elements elements) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer  = new FileWriter("new.csv");

        StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();

        List<ItemAliexpress> itemAliexpressList = new ArrayList<>();

        for(int i=0;i<100;i++) {
            String id = elements.get(i).parentNode().parentNode().attr("exp_product").toString();
            String title=elements.get(i).childNodes().get(0).attr("title").toString();
            String price=elements.get(i).childNodes().get(1).childNodes().get(0).attr("title").toString();
            String sold =elements.get(i).childNodes().get(2).childNodes().get(0).childNodes().get(0).toString();
            if(price=="")
                price=elements.get(i).childNodes().get(1).childNodes().get(1).attr("title").toString();
            String link =elements.get(i).childNodes().get(0).childNodes().get(0).attr("href").toString();
            ItemAliexpress itemAliexpress=new ItemAliexpress(id,title,price,link,sold);
            itemAliexpressList.add(itemAliexpress);
        }
        sbc.write(itemAliexpressList);
        writer.close();

        int s=0;

    }
}
