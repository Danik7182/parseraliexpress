package domain;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import java.math.BigDecimal;


public class AbstractItem{

    @CsvBindByName()
     private String id;
    @CsvBindByName()
    private String title;

    @CsvBindByName()
    private String price;

    @CsvBindByName()
    private String url;

    public AbstractItem() {
    }

    public AbstractItem(String id, String title, String price,String url) {
        this.id = id;
        this.title = title;

        this.price = price;
        this.url=url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "AbstractItem{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
