package domain;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import java.math.BigDecimal;

public class ItemAliexpress extends AbstractItem {

    @CsvBindByName()
    private String soldCounter;



    public ItemAliexpress(String id, String title, String price, String url, String soldCounter) {
        super(id, title, price,url);

        this.soldCounter=soldCounter;
    }

    public ItemAliexpress() {
    }

    public String getSoldCounter() {
        return soldCounter;
    }

    public void setSoldCounter(String soldCounter) {
        this.soldCounter = soldCounter;
    }

    @Override
    public String toString() {
        return "ItemAliexpress{" +

                ", soldCounter=" + soldCounter +
                '}';
    }
}
